#~
 # Copyright 2022 ACES.
 #
 # This is free software; you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation; either version 3, or (at your option)
 # any later version.
 #
 # This software is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this software; see the file COPYING.  If not, write to
 # the Free Software Foundation, Inc., 51 Franklin Street,
 # Boston, MA 02110-1301, USA.
 #~
#~ \file nyquist_example.py #~
#~ imported modules #~
import time
import numpy as np
from nyquist.lab import System
from nyquist.control import Experiment

DUTY_HIGH = 35   #~ \brief   High duty value. #~
DUTY_LOW = 25   #~ \brief    Low duty value. #~
DUTY_OFF = 0   #~ \brief    Zero duty value. #~
ANGLE_SETPOINT = 40 #~ \brief   Setpoint angle value. #~
ENCODER_VALUE_DEFAULT = 0   #~ \brief   Default value for encoder initialization. #~
SAMPLING_PERIOD_MS = 10 #~ \brief   Sampling period in miliseconds for telemetry. #~
LOOP_FREQ = 20  #~ \brief   Loop frequence. #~
RUN_TIME = 6    #~ \brief   Run time. #~
LOOP_TIME = 1   #~ \brief   Loop time. #~

#~ System parameters. #~
sys = "motor-encoder"
ip = "192.168.50.20"

class MyExperiment(Experiment):

    #~
     # \brief   Function for system
     #          initialization.
     #~
    def before_the_loop(self):
        self.device = System(sys, ip)
        if self.device.motor.state.get() == "STATE_OFF":
            # self.device.telemetry.period.post(SAMPLING_PERIOD_MS)
            self.device.encoder.value.post(ENCODER_VALUE_DEFAULT)
            self.device.motor.state.post("STATE_ON")
            self.device.motor.rotation.post("ROTATION_CW")
        self.angle = []
        self.time = []
        self.start_ts = time.monotonic()

    #~
     # \brief   Function to perform
     #          control loop.
     #~
    def in_the_loop(self):
        if self.device.stream.angle._Endpoint__resourcer.new_message:
            angle = self.device.stream.angle.get() 
            self.time.append(time.monotonic() - self.start_ts)
            self.angle.append(angle)
            print(angle)
            if angle is not None:
                if angle < self.setpoint_deg:
                    self.device.steam.duty.post(self.duty_high)
                else:
                    self.device.steam.duty.post(self.duty_low)
    
    #~
     # \brief   Function to shut down
     #          the system for safety.
     #~
    def after_the_loop(self):
        self.device.steam.duty.post(DUTY_OFF)
        self.device.motor.state.post("STATE_OFF")
        self.device.encoder.value.post(ENCODER_VALUE_DEFAULT)

exp = MyExperiment()    #~ \brief   Instance experiment. ~#

#~
 # \note   System constants
 #         are set.
 #~
exp.setpoint_deg = ANGLE_SETPOINT
exp.duty_high = DUTY_HIGH
exp.duty_low = DUTY_LOW
exp.set_loop_frequency(LOOP_FREQ)
exp.set_run_time(RUN_TIME)
exp.set_before_loop_time(LOOP_TIME)
#~ system run #~
exp.run()

print(exp.time)
print(np.sin(np.deg2rad(exp.angle)))
