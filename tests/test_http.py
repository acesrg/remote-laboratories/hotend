from esplab import Resourcer
from socket import timeout
import unittest

# TODO: this should be more automatic
IP_ADD = "192.168.50.10"

class HTTPTestCase(unittest.TestCase):
    def setUp(self):
        retries = 3
        while retries >= 0:
            try:
                self.resourcer = Resourcer(IP_ADD, port=80, timeout=20)
                break
            except timeout:
                print("Timed out, retrying...")
                retries -= 1

    def _test_resources(self, resource, value):
        RESOURCE = resource
        VALUE = value
        value_type = type(value)
        response = self.resourcer.post(RESOURCE, VALUE)
        self.assertEqual(response, 202)

        response = self.resourcer.get(RESOURCE)
        self.assertEqual(value_type(response), VALUE)

    def test_logger(self):
        """ 
        Logger sentence for HTTP it's working!
        """
        RESOURCE = "/logger/level"
        LOG_LEVEL = "LOG_TRACE"
        self._test_resources(RESOURCE, LOG_LEVEL)
        LOG_LEVEL = "LOG_INFO"
        self._test_resources(RESOURCE, LOG_LEVEL)

    def test_motor_state(self):
        """ 
        Motor state sentence for HTTP it's working!
        """
        RESOURCE = "/motor/state"
        DEFAULT_STATE = "STATE_OFF"
        self._test_resources(RESOURCE, DEFAULT_STATE)
        self._test_resources(RESOURCE, "STATE_ON")
        self._test_resources(RESOURCE, DEFAULT_STATE)
       
    def test_motor_rotation(self):
        """ 
        Motor rotation sentence for HTTP it's working!
        """
        RESOURCE = "/motor/rotation"
        DEFAULT_ROTATION = "ROTATION_CW"
        self._test_resources(RESOURCE, DEFAULT_ROTATION)
        self._test_resources(RESOURCE, "ROTATION_CCW")
        self._test_resources(RESOURCE, DEFAULT_ROTATION)

    def test_telemetry_period(self):
        """ 
        Telemetry period sentence for HTTP it's working!
        """
        RESOURCE = "/telemetry/period"
        self._test_resources(RESOURCE, "20")
        DEFAULT_PERIOD = 500
        self._test_resources(RESOURCE, DEFAULT_PERIOD)

    def _test_resources_encoder(self, resource, value):
        RESOURCE = resource
        VALUE = value
        value_type = type(value)
        response = self.resourcer.post(RESOURCE, VALUE)
        self.assertEqual(response, 202)

    def test_encoder(self):
        """ 
        Encoder sentence for HTTP it's working!
        """
        RESOURCE = "/encoder/value"
        DEFAULT_VALUE = 0
        self._test_resources_encoder(RESOURCE, DEFAULT_VALUE)
