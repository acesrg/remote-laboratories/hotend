/*
 * Copyright 2022 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file motor.h */
#ifndef SRC_LIB_MOTOR_INC_MOTOR_H_
#define SRC_LIB_MOTOR_INC_MOTOR_H_

/* third party local libs*/
#include <retval.h>

typedef enum {
    ROT_CW,
    ROT_CCW,
} motor_rotation_t;

retval_t motor_init();
retval_t motor_deinit();
retval_t motor_rotation(motor_rotation_t rotation);
retval_t motor_set_duty(uint16_t value);

#endif  /* SRC_LIB_MOTOR_INC_MOTOR_H_ */
