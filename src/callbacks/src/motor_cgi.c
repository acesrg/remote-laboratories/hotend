/*
 * Copyright 2022 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file motor_cgi.c */
/* third party libs */
#include <httpd/httpd.h>

/* third party local libs */
#include <log.h>
#include <cgi_utils.h>
#include <ssi_utils.h>

/* proyect callbacks/cgi */
#include <motor_cgi.h>

/* local libs */
#include <motor.h>

typedef enum MotorState {
    STATE_OFF,
    STATE_ON
} MotorState_t;

typedef enum MotorRotation {
    ROTATION_CW,
    ROTATION_CCW
} MotorRotation_t;

MotorState_t motor_state;

MotorRotation_t motor_rot;

/**
 * \brief   Function to use the motor handler and set the motor state. 
 * \param   iIndex: Unused.
 * \param   iNumParams: Quantity of variables in the query.
 * \param   *pcParam[]: Pointer to first char of first string in query-names array.
 * \param   *pcValue[]: Pointer to first char of first string in query-values array.
 * \return  A file name, which will contain an HTTP return code.
 */
const char *motor_state_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]) {
    uriParamsType motor_cgi_params[2] = {{"verb", "0", false}, {"value", "0", false}};

    retval_t rv;

    rv = decode_uri_params(
        iNumParams,
        (char **) pcParam,
        (char **) pcValue,
        sizeof(motor_cgi_params) / sizeof(motor_cgi_params[0]),
        motor_cgi_params);

    if (rv != RV_OK) {
        return HTTP_CODE(500);
    }

    char param_verb[URI_VARIABLE_VALUE_MAX_LEN];

    rv = uri_param_read(
        "verb",
        param_verb,
        motor_cgi_params,
        sizeof(motor_cgi_params) / sizeof(motor_cgi_params[0]));

    if (rv != RV_OK) {
        return HTTP_CODE(400);
    }

    switch (decode_http_verb(param_verb)) {
        case GET: {
            switch (motor_state) {
                case STATE_OFF:
                    load_ssi_data("STATE_OFF", sizeof("STATE_OFF"));
                    break;
                case STATE_ON:
                    load_ssi_data("STATE_ON", sizeof("STATE_ON"));
                    break;
                default:
                    return HTTP_CODE(400);
            }
            return "/response.ssi";
        }

        case POST: {
            char param_value[URI_VARIABLE_VALUE_MAX_LEN];
            rv = uri_param_read(
                    "value",
                    param_value,
                    motor_cgi_params,
                    sizeof(motor_cgi_params) / sizeof(motor_cgi_params[0]));

            if (rv != RV_OK) {
                return HTTP_CODE(400);
            }

            if (are_strings_equal(param_value, "STATE_ON")) {
                motor_init();
                motor_state = STATE_ON;
                return HTTP_CODE(202);
            } else if (are_strings_equal(param_value, "STATE_OFF")) {
                motor_deinit();
                motor_state = STATE_OFF;
                return HTTP_CODE(202);
            }
            return HTTP_CODE(400);
        }
        default:
            return HTTP_CODE(400);
    }
}

/**
 * \brief   Function to use the motor's handler and set the sense of the motor rotation. 
 * \param   iIndex: Unused.
 * \param   iNumParams: Quantity of variables in the query.
 * \param   *pcParam[]: Pointer to first char of first string in query-names array.
 * \param   *pcValue[]: Pointer to first char of first string in query-values array.
 * \return  A file name, which will contain an HTTP return code.
 */
const char *motor_rotation_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]) {
    uriParamsType motor_cgi_params[2] = {{"verb", "0", false}, {"value", "0", false}};

    retval_t rv;

    rv = decode_uri_params(
        iNumParams,
        (char **) pcParam,
        (char **) pcValue,
        sizeof(motor_cgi_params) / sizeof(motor_cgi_params[0]),
        motor_cgi_params);

    if (rv != RV_OK) {
        return HTTP_CODE(500);
    }

    char param_verb[URI_VARIABLE_VALUE_MAX_LEN];

    rv = uri_param_read(
        "verb",
        param_verb,
        motor_cgi_params,
        sizeof(motor_cgi_params) / sizeof(motor_cgi_params[0]));

    if (rv != RV_OK) {
        return HTTP_CODE(400);
    }

    switch (decode_http_verb(param_verb)) {
        case GET: {
            switch (motor_rot) {
                case ROTATION_CW:
                    load_ssi_data("ROTATION_CW", sizeof("ROTATION_CW"));
                    break;
                case ROTATION_CCW:
                    load_ssi_data("ROTATION_CCW", sizeof("ROTATION_CCW"));
                    break;
                default:
                    return HTTP_CODE(400);
            }
            return "/response.ssi";
        }

        case POST: {
            char param_value[URI_VARIABLE_VALUE_MAX_LEN];
            rv = uri_param_read(
                    "value",
                    param_value,
                    motor_cgi_params,
                    sizeof(motor_cgi_params) / sizeof(motor_cgi_params[0]));

            if (rv != RV_OK) {
                return HTTP_CODE(400);
            }

            if (are_strings_equal(param_value, "ROTATION_CW")) {
                motor_rotation(ROT_CW);
                motor_rot = ROTATION_CW;
                return HTTP_CODE(202);
            } else if (are_strings_equal(param_value, "ROTATION_CCW")) {
                motor_rotation(ROT_CCW);
                motor_rot = ROTATION_CCW;
                return HTTP_CODE(202);
            }
            return HTTP_CODE(400);
        }
        default:
            return HTTP_CODE(400);
    }
}
