/*
 * Copyright 2022 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file encoder_cgi.c */
/* third party libs */
#include <httpd/httpd.h>

/* third party local libs */
#include <log.h>
#include <cgi_utils.h>
#include <ssi_utils.h>

/* proyect callbacks/cgi */
#include <encoder_cgi.h>

/* local libs */
#include <encoder.h>

/**
 * \brief   Function to use the encoder handler and set the encoder value.
 * \param   iIndex: unused
 * \param   iNumParams: quantity of variables in the query
 * \param   *pcParam[]: pointer to first char of first string in query-names array
 * \param   *pcValue[]: pointer to first char of first string in query-values array
 * \return  a file name, which will contain an HTTP return code
 */
const char *encoder_value_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]) {
    uriParamsType encoder_cgi_params[2] = {{"verb", "0", false}, {"value", "0", false}};

    retval_t rv;

    rv = decode_uri_params(
        iNumParams,
        (char **) pcParam,
        (char **) pcValue,
        sizeof(encoder_cgi_params) / sizeof(encoder_cgi_params[0]),
        encoder_cgi_params);

    if (rv != RV_OK) {
        return HTTP_CODE(500);
    }

    char param_verb[URI_VARIABLE_VALUE_MAX_LEN];

    rv = uri_param_read(
        "verb",
        param_verb,
        encoder_cgi_params,
        sizeof(encoder_cgi_params) / sizeof(encoder_cgi_params[0]));

    if (rv != RV_OK) {
        return HTTP_CODE(400);
    }

    switch (decode_http_verb(param_verb)) {
        case GET: {
            return HTTP_CODE(501);
        }

        case POST: {
            char param_value[URI_VARIABLE_VALUE_MAX_LEN];
            rv = uri_param_read(
                "value",
                param_value,
                encoder_cgi_params,
                sizeof(encoder_cgi_params) / sizeof(encoder_cgi_params[0]));

            if (rv != RV_OK) {
                return HTTP_CODE(400);
            }

            int value_int = atoi(param_value);
            set_encoder_value(value_int);
            return HTTP_CODE(202);
        }

        default:
            return HTTP_CODE(400);
    }
}
